package tutorial.itp341.com.parselogin;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by arjunpola on 15/11/15.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "", "");
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }
}
